package com.example.banco;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.activity.EdgeToEdge;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.graphics.Insets;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;

public class MainActivity extends AppCompatActivity {

    private Button btnIngresar, btnSalir;
    private EditText txtUsuario, txtPassword;

    private TextView txtDomicilio, txtEmpresa;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EdgeToEdge.enable(this);
        setContentView(R.layout.activity_main);

        iniciarComponentes();

        btnIngresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(txtUsuario.getText().toString().matches("")){
                    Toast.makeText(MainActivity.this, "Capture el usuario", Toast.LENGTH_SHORT).show();
                }else if (!txtUsuario.getText().toString().matches(getString(R.string.user))){
                    Toast.makeText(MainActivity.this, "Datos Incorrectos", Toast.LENGTH_SHORT).show();

                }else if(!txtPassword.getText().toString().matches(getString(R.string.pass))){

                    Toast.makeText(MainActivity.this, "Contraseña Incorrecta", Toast.LENGTH_SHORT).show();
                }  else {

                    txtUsuario.setText("");
                    txtPassword.setText("");
                    Intent intent = new Intent(getApplicationContext(),
                            CuentaBancoActivity.class);
                    intent.putExtra("cliente",txtUsuario.getText().toString());
                    startActivity(intent);
                }
            }
        });

        this.btnSalir.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main), (v, insets) -> {
            Insets systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars());
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom);
            return insets;
        });
    }

    public void iniciarComponentes(){
        btnIngresar = (Button) findViewById(R.id.btnIngresar);
        btnSalir = (Button) findViewById(R.id.btnSalir);
        txtUsuario = (EditText) findViewById(R.id.txtUsuario);
        txtPassword = (EditText) findViewById(R.id.txtPassword);
        txtEmpresa = findViewById(R.id.txtEmpresa);
        txtDomicilio = findViewById(R.id.txtDomicilio);

        txtDomicilio.setText(getText(R.string.domicilio));
        txtEmpresa.setText(getText(R.string.banco));


    }

}